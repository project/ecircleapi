SETUP

- account with eCircle (!!)
- download and install nusoap in the module/lib folder
- update settings admin/settings/ecircleapi 


CURRENT LIMITATIONS WITH ECIRCLE

Changing Emails
Email addresses cannot be changed in eCircle, if a user changes their email in 
Drupal they are instantly disjointed. No solution has been developed for this.
An interim step could be to trigger off a changes email for someone to manually amend ECircle.

Group listing
With eCircle there is no way to look up how many groups are in a given installation

Groups per user
When a user object is returned it doesn't display groups per user. 
Hence the need to effectively duplicate the groups a user is member of in Drupal. 
The alternative is to load the eCircle object on user load, which would only slow the site further.


FULL API
Refer to http://webservices.ecircle-ag.com/soap/soapdoc/api.html
for a full list of available API Operations


MISC DOCS

Explains the structure on ec-messenger in 5 mins
http://developer.ecircle-ag.com/apiwiki/wiki/EcmessengerIn5Minutes

Overview of sync
http://developer.ecircle-ag.com/apiwiki/wiki/SynchronousSoapAPI

different types of sync soap calls
http://webservices.ecircle-ag.com/soap/soapdoc/api.html

a good site for testing different soap calls
http://www.soapclient.com/soapclient?template=%2Fclientform.html&fn=soapform&SoapTemplate=%2FSoapResult.html&SoapWSDL=http%3A%2F%2Fwebservices.ecircle-ag.com%2Frpc%3Fwsdl&_ArraySize=2

the site above explained
http://developer.ecircle-ag.com/apiwiki/wiki/SoapClientCom